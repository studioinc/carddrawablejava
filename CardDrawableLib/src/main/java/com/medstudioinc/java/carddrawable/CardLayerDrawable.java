package com.medstudioinc.java.carddrawable;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;

import com.medstudioinc.java.carddrawable.enums.BackgroundColorType;
import com.medstudioinc.java.carddrawable.enums.GradientType;
import com.medstudioinc.java.carddrawable.enums.OrientationType;
import com.medstudioinc.java.carddrawable.enums.SelectorStateMode;
import com.medstudioinc.java.carddrawable.enums.ShapeType;
import com.medstudioinc.java.carddrawable.utils.DisplayUtility;
import com.medstudioinc.java.carddrawable.utils.DrawUtils;


/**
 * Author : Mohammmed Ajaroud
 * Birthday : 27/09/1992
 * Github : https://www.github.com/mohammedajaroud
 * Created At : 11/10/2018 - 13:15h PM
 * OS Version : Mac OS X Hight Sierra
 * Android Studio : 3.1.4
 * Target SDK : 28.0.0
 * Languages : Java/Kotlin
 */


@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
public class CardLayerDrawable {
    private Context context;
    private View viewGroup;

    private int backgroundColor;
    private int borderSolidColor;
    private int[] borderGradientColor;
    private int rippleColor;

    private int stokeColor;
    private int stokeWidth;
    private int[] normalColors;
    private int[] pressedColors;
    private float cornersRadius;
    private float gradientRadius;

    private int left;
    private int top;
    private int right;
    private int bottom;

    private GradientType gradientType;
    private ShapeType shapeType;
    private OrientationType orientationType;
    private BackgroundColorType backgroundColorType;
    private SelectorStateMode selectorStateMode;

    public CardLayerDrawable(Context context) {
        this.context = context;
        defaultValues();
    }

    private void defaultValues(){
        this.backgroundColor = Color.parseColor("#FFFFFF");
        this.borderSolidColor = Color.parseColor("#69d8d8d8");
        this.borderGradientColor = new int[] {Color.parseColor("#69d8d8d8"), Color.parseColor("#EAEAEA")};
        this.rippleColor = Color.parseColor("#FFFFFF");
        this.stokeColor = Color.parseColor("#EAEAEA");

        this.stokeWidth = DisplayUtility.dp2px(context, 0);

        this.normalColors = new int[] {Color.RED, Color.BLACK, Color.BLUE};
        this.pressedColors = new int[] {Color.GRAY, Color.GREEN, Color.DKGRAY};

        this.cornersRadius = 0f;
        this.gradientRadius = 0f;

        this.gradientType = GradientType.LINEAR_GRADIENT;
        this.shapeType = ShapeType.RECTANGLE;
        this.orientationType = OrientationType.TOP_BOTTOM;
        this.backgroundColorType = BackgroundColorType.SOLID;
        this.selectorStateMode = SelectorStateMode.NORMAL;
    }

    public static CardLayerDrawable fit(Context context){
        return new CardLayerDrawable(context);
    }

    public CardLayerDrawable setView(View view){
        this.viewGroup = view;
        return this;
    }

    public CardLayerDrawable setBackgroundColorType(BackgroundColorType backgroundColorType){
        this.backgroundColorType = backgroundColorType;
        return this;
    }

    public CardLayerDrawable setNormalColors(int[] colors){
        this.normalColors = colors;
        return this;
    }

    public CardLayerDrawable setPressedColors(int[] colors){
        this.pressedColors = colors;
        return this;
    }

    public CardLayerDrawable setBackgroundColor(int color){
        this.backgroundColor = color;
        return this;
    }

    public CardLayerDrawable setBorderSolidColor(int color){
        this.borderSolidColor = color;
        return this;
    }

    public CardLayerDrawable setBorderGradientColor(int[] colors){
        this.borderGradientColor = colors;
        return this;
    }

    public CardLayerDrawable setRippleColor(int color){
        this.rippleColor = color;
        return this;
    }

    public CardLayerDrawable setStokeColor(int color){
        this.stokeColor = color;
        return this;
    }

    public CardLayerDrawable setStokeWidth(float width){
        this.stokeWidth = DisplayUtility.dp2px(context, width);
        return this;
    }

    public CardLayerDrawable setCornersRadius(float cornersRadius){
        this.cornersRadius = cornersRadius;
        return this;
    }

    public CardLayerDrawable setGradientRadius(float gradientRadius){
        this.gradientRadius = gradientRadius;
        return this;
    }

    public CardLayerDrawable setGradientType(GradientType gradientType){
        this.gradientType = gradientType;
        return this;
    }

    public CardLayerDrawable setShapeType(ShapeType shapeType){
        this.shapeType = shapeType;
        return this;
    }

    public CardLayerDrawable setOrientationType(OrientationType orientationType){
        this.orientationType = orientationType;
        return this;
    }


    public CardLayerDrawable setLeft(int left){
        this.left = left;
        return this;
    }

    public CardLayerDrawable setTop(int top){
        this.top = top;
        return this;
    }

    public CardLayerDrawable setRight(int right){
        this.right = right;
        return this;
    }

    public CardLayerDrawable setBottom(int bottom){
        this.bottom = bottom;
        return this;
    }

    // draw
    public void draw(){
        // Set GradientDrawable as ImageView source image
        if (viewGroup != null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                RippleDrawable rippleDrawable = DrawUtils.getBackgroundDrawable(rippleColor, new RippleDrawable(DrawUtils.getPressedState(rippleColor), DrawUtils.getSelector(
                        getDrawable(SelectorStateMode.NORMAL),
                        getDrawable(SelectorStateMode.NORMAL)), null));

                this.viewGroup.setBackground(rippleDrawable);
            } else {
                this.viewGroup.setBackground(DrawUtils.getSelector(getDrawable(SelectorStateMode.NORMAL), getDrawable(SelectorStateMode.PRESSED)));
            }
        }
    }



    //GradientDrawable instead Drawable
    public Drawable getDrawable(SelectorStateMode selectorStateMode) {
        this.selectorStateMode = selectorStateMode;
        // Initialize a new GradientDrawable
        GradientDrawable background = new GradientDrawable();
        GradientDrawable border = new GradientDrawable();

        //Sets the radius of the gradient.
        background.setGradientRadius(gradientRadius);
        border.setGradientRadius(gradientRadius);

        //Sets the radius of the corner.
        background.setCornerRadius(cornersRadius);
        border.setCornerRadius(cornersRadius);


        // Set pixels width solid stoke color border
        background.setStroke(stokeWidth, stokeColor, 8, 5);//设置边框厚度和颜色
        border.setStroke(stokeWidth, stokeColor);//设置边框厚度和颜色


        // Set the color array to draw gradient
        setBackgroundColorType(new GradientDrawable[]{background, border});

        // Set GradientDrawable shape is a rectangle
        setShape(new GradientDrawable[]{background, border}, this.shapeType);

        // Set the GradientDrawable orientation
        setOrientation(new GradientDrawable[]{background, border}, this.orientationType);

        // Set the GradientDrawable gradient type linear gradient
        setGradientType(new GradientDrawable[]{background, border}, this.gradientType);


        Drawable[] layers = {background, border};
        LayerDrawable layerDrawable = new LayerDrawable(layers);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            layerDrawable.setPadding(16, 8, 16, 8);
        }

        layerDrawable.setLayerInset(0, 0, 0, 0, 0);
        layerDrawable.setLayerInset(1, left, top, right, bottom);

        /*
           Sets the center location of the gradient.
           setGradientCenter(float x, float y)
           //background.setGradientCenter(0,0);
        */

        // Set GradientDrawable width and in pixels
        //background.setSize(200, 200); // Width 400 pixels and height 100 pixels
        return layerDrawable;

    }
    private void setOrientation(GradientDrawable[] drawables, OrientationType orientationType){
        switch (orientationType){
            case TOP_BOTTOM:
                drawables[1].setOrientation(GradientDrawable.Orientation.TOP_BOTTOM);
                break;
            case TR_BL:
                drawables[1].setOrientation(GradientDrawable.Orientation.TR_BL);
                break;
            case RIGHT_LEFT:
                drawables[1].setOrientation(GradientDrawable.Orientation.RIGHT_LEFT);
                break;
            case BR_TL:
                drawables[1].setOrientation(GradientDrawable.Orientation.BR_TL);
                break;
            case BOTTOM_TOP:
                drawables[1].setOrientation(GradientDrawable.Orientation.BOTTOM_TOP);
                break;
            case LEFT_RIGHT:
                drawables[1].setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);
                break;
            case BL_TR:
                drawables[1].setOrientation(GradientDrawable.Orientation.BL_TR);
                break;
        }
    }

    private void setGradientType(GradientDrawable[] drawables, GradientType gradientType){
        switch (gradientType){
            case LINEAR_GRADIENT:
                drawables[1].setGradientType(GradientDrawable.LINEAR_GRADIENT);
                break;
            case RADIAL_GRADIENT:
                drawables[1].setGradientType(GradientDrawable.RADIAL_GRADIENT);
                break;
            case SWEEP_GRADIENT:
                drawables[1].setGradientType(GradientDrawable.SWEEP_GRADIENT);
                break;
        }
    }

    private void setBackgroundColorType(GradientDrawable[] drawables){
        switch (this.backgroundColorType){
            case SOLID:
                drawables[0].setColor(borderSolidColor);
                switch (selectorStateMode){
                    case NORMAL:
                        drawables[1].setColor(backgroundColor);
                        break;
                    case PRESSED:
                        drawables[1].setColor(rippleColor);
                        break;
                }
                break;
            case GRADIENT:
                drawables[0].setColors(borderGradientColor);
                switch (selectorStateMode){
                    case NORMAL:
                        drawables[1].setColors(normalColors);
                        break;
                    case PRESSED:
                        drawables[1].setColors(pressedColors);
                        break;
                }
                break;
        }
    }

    private void setShape(GradientDrawable[] drawables, ShapeType shapeType){
        switch (shapeType){
            case RECTANGLE:
                drawables[0].setShape(GradientDrawable.RECTANGLE);
                break;
            case OVAL:
                drawables[0].setShape(GradientDrawable.OVAL);
                break;
            case LINE:
                drawables[0].setShape(GradientDrawable.LINE);
                break;
            case RING:
                drawables[0].setShape(GradientDrawable.RING);
                break;
        }
    }

}
