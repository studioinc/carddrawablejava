package com.medstudioinc.java.carddrawable;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;

import com.medstudioinc.java.carddrawable.enums.GradientType;
import com.medstudioinc.java.carddrawable.enums.OrientationType;
import com.medstudioinc.java.carddrawable.enums.ShapeType;
import com.medstudioinc.java.carddrawable.utils.DisplayUtility;
import com.medstudioinc.java.carddrawable.utils.DrawUtils;


@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
public class LayerDrawableUtility {
    private Context context;
    private View viewGroup;
    private int backgroundColor;
    private int borderColor;
    private int rippleColor;
    private int stokeColor;
    private int stokeWidth;
    private int[] normalColors;
    private int[] pressedColors;
    private float cornersRadius;
    private float gradientRadius;

    private int left;
    private int top;
    private int right;
    private int bottom;

    private GradientType gradientType;
    private ShapeType shapeType;
    private OrientationType orientationType;

    public LayerDrawableUtility(Context ctx) {
        this.context = ctx;
        defaultValues();
    }

    private void defaultValues(){
        this.backgroundColor = Color.parseColor("#FFFFFF");
        this.borderColor = Color.parseColor("#69d8d8d8");
        this.rippleColor = Color.parseColor("#FFFFFF");
        this.stokeColor = Color.parseColor("#EAEAEA");

        this.stokeWidth = DisplayUtility.dp2px(context, 1);

        this.left = 1;
        this.top = 1;
        this.right = 1;
        this.bottom = 3;

        this.normalColors = new int[] {Color.RED, Color.BLACK, Color.BLUE};
        this.cornersRadius = 0F;
        this.gradientRadius = 0F;
        this.gradientType = GradientType.LINEAR_GRADIENT;
        this.shapeType = ShapeType.RECTANGLE;
        this.orientationType = OrientationType.TOP_BOTTOM;
    }

    public static LayerDrawableUtility fit(Context ctx){
        return new LayerDrawableUtility(ctx);
    }

    public LayerDrawableUtility setView(View view){
        this.viewGroup = view;
        return this;
    }

    public LayerDrawableUtility setNormalColors(int[] colors){
        this.normalColors = colors;
        return this;
    }

    public LayerDrawableUtility setPressedColors(int[] colors){
        this.pressedColors = colors;
        return this;
    }

    public LayerDrawableUtility setBackgroundColor(int color){
        this.backgroundColor = color;
        return this;
    }

    public LayerDrawableUtility setBorderColor(int color){
        this.borderColor = color;
        return this;
    }

    public LayerDrawableUtility setRippleColor(int color){
        this.rippleColor = color;
        return this;
    }

    public LayerDrawableUtility setStokeColor(int color){
        this.stokeColor = color;
        return this;
    }

    public LayerDrawableUtility setStokeWidth(int width){
        this.stokeWidth = width;
        return this;
    }

    public LayerDrawableUtility setCornersRadius(float cornersRadius){
        this.cornersRadius = cornersRadius;
        return this;
    }

    public LayerDrawableUtility setGradientRadius(float gradientRadius){
        this.gradientRadius = gradientRadius;
        return this;
    }

    public LayerDrawableUtility setGradientType(GradientType gradientType){
        this.gradientType = gradientType;
        return this;
    }

    public LayerDrawableUtility setShapeType(ShapeType shapeType){
        this.shapeType = shapeType;
        return this;
    }

    public LayerDrawableUtility setOrientationType(OrientationType orientationType){
        this.orientationType = orientationType;
        return this;
    }

    public LayerDrawableUtility setLeft(int left){
        this.left = left;
        return this;
    }

    public LayerDrawableUtility setTop(int top){
        this.top = top;
        return this;
    }

    public LayerDrawableUtility setRight(int right){
        this.right = right;
        return this;
    }

    public LayerDrawableUtility setBottom(int bottom){
        this.bottom = bottom;
        return this;
    }

    public void draw(){
        GradientDrawable border = new GradientDrawable();
        border.setColor(borderColor);
        border.setGradientRadius(gradientRadius);
        border.setCornerRadius(cornersRadius);

        GradientDrawable background = new GradientDrawable();
        background.setColor(backgroundColor);
        background.setGradientRadius(gradientRadius);
        background.setCornerRadius(cornersRadius);


        setShape(new GradientDrawable[]{border, background}, this.shapeType);

        setOrientation(new GradientDrawable[]{border, background}, this.orientationType);
        // Set the GradientDrawable gradient type linear gradient
        setGradientType(new GradientDrawable[]{border, background}, this.gradientType);


        Drawable[] layers = {border, background};
        LayerDrawable layerDrawable = new LayerDrawable(layers);

        layerDrawable.setLayerInset(0, 0, 0, 0, 0);
        layerDrawable.setLayerInset(1, left, top, right, bottom);

        // Set GradientDrawable as ImageView source image
        if (viewGroup != null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                RippleDrawable rippleDrawable = DrawUtils.getBackgroundDrawable(rippleColor, new RippleDrawable(DrawUtils.getPressedState(rippleColor), layerDrawable, null));
                this.viewGroup.setBackground(rippleDrawable);
            } else {
                this.viewGroup.setBackground(layerDrawable);
            }
        }
    }

    private void setOrientation(GradientDrawable[] drawables, OrientationType orientationType){
        switch (orientationType){
            case TOP_BOTTOM:
                drawables[0].setOrientation(GradientDrawable.Orientation.TOP_BOTTOM);
                drawables[1].setOrientation(GradientDrawable.Orientation.TOP_BOTTOM);
                break;
            case TR_BL:
                drawables[0].setOrientation(GradientDrawable.Orientation.TR_BL);
                drawables[1].setOrientation(GradientDrawable.Orientation.TR_BL);
                break;
            case RIGHT_LEFT:
                drawables[0].setOrientation(GradientDrawable.Orientation.RIGHT_LEFT);
                drawables[1].setOrientation(GradientDrawable.Orientation.RIGHT_LEFT);
                break;
            case BR_TL:
                drawables[0].setOrientation(GradientDrawable.Orientation.BR_TL);
                drawables[1].setOrientation(GradientDrawable.Orientation.BR_TL);
                break;
            case BOTTOM_TOP:
                drawables[0].setOrientation(GradientDrawable.Orientation.BOTTOM_TOP);
                drawables[1].setOrientation(GradientDrawable.Orientation.BOTTOM_TOP);
                break;
            case LEFT_RIGHT:
                drawables[0].setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);
                drawables[1].setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);
                break;
            case BL_TR:
                drawables[0].setOrientation(GradientDrawable.Orientation.BL_TR);
                drawables[1].setOrientation(GradientDrawable.Orientation.BL_TR);
                break;
        }
    }

    private void setGradientType(GradientDrawable[] drawables, GradientType gradientType){
        switch (gradientType){
            case LINEAR_GRADIENT:
                drawables[0].setGradientType(GradientDrawable.LINEAR_GRADIENT);
                drawables[1].setGradientType(GradientDrawable.LINEAR_GRADIENT);
                break;
            case RADIAL_GRADIENT:
                drawables[0].setGradientType(GradientDrawable.RADIAL_GRADIENT);
                drawables[1].setGradientType(GradientDrawable.RADIAL_GRADIENT);
                break;
            case SWEEP_GRADIENT:
                drawables[0].setGradientType(GradientDrawable.SWEEP_GRADIENT);
                drawables[1].setGradientType(GradientDrawable.SWEEP_GRADIENT);
                break;
        }
    }

    private void setShape(GradientDrawable[] drawables, ShapeType shapeType){
        switch (shapeType){
            case RECTANGLE:
                drawables[0].setShape(GradientDrawable.RECTANGLE);
                drawables[1].setShape(GradientDrawable.RECTANGLE);
                break;
            case OVAL:
                drawables[0].setShape(GradientDrawable.OVAL);
                drawables[1].setShape(GradientDrawable.OVAL);
                break;
            case LINE:
                drawables[0].setShape(GradientDrawable.LINE);
                drawables[1].setShape(GradientDrawable.LINE);
                break;
            case RING:
                drawables[0].setShape(GradientDrawable.RING);
                drawables[1].setShape(GradientDrawable.RING);
                break;
        }
    }
}
