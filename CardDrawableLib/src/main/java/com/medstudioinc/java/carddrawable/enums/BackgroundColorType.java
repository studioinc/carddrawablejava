package com.medstudioinc.java.carddrawable.enums;

public enum BackgroundColorType {
    SOLID,
    GRADIENT
}
