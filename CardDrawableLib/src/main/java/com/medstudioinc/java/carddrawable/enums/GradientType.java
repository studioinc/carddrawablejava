package com.medstudioinc.java.carddrawable.enums;

public enum GradientType {
    LINEAR_GRADIENT,
    RADIAL_GRADIENT,
    SWEEP_GRADIENT
}