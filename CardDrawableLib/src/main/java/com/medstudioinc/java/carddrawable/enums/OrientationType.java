package com.medstudioinc.java.carddrawable.enums;

public enum OrientationType {
    TOP_BOTTOM,
    TR_BL,
    RIGHT_LEFT,
    BR_TL,
    BOTTOM_TOP,
    BL_TR,
    LEFT_RIGHT,
    TL_BR
}