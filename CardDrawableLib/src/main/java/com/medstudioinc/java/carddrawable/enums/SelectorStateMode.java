package com.medstudioinc.java.carddrawable.enums;

public enum SelectorStateMode {
    NORMAL,
    PRESSED
}
