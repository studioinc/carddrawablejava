package com.medstudioinc.java.carddrawable.enums;

public enum ShapeType {
    RECTANGLE,
    OVAL,
    LINE,
    RING
}