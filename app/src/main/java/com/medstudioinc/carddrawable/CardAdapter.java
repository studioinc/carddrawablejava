package com.medstudioinc.carddrawable;

import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.medstudioinc.java.carddrawable.CardLayerDrawable;
import com.medstudioinc.java.carddrawable.enums.BackgroundColorType;
import com.medstudioinc.java.carddrawable.enums.GradientType;
import com.medstudioinc.java.carddrawable.enums.OrientationType;
import com.medstudioinc.java.carddrawable.enums.ShapeType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
public class CardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<CardModel> cardModels = new ArrayList<>();
    String[][] arrayNormalColors;
    String[][] arrayPressedColors;


    public CardAdapter() {
    }

    public void addCards(ArrayList<CardModel> cardModels){
        for (CardModel cardModel : cardModels){
            add(cardModel);
        }

        addColors();
    }

    public void add(CardModel cardModel){
        cardModels.add(cardModel);
        notifyItemInserted(cardModels.size() -1);
    }

    public void addColors(){
        HashMap<String, String> hashMap = new HashMap<>();
        arrayNormalColors = new String[getItemCount()][2];
        arrayPressedColors = new String[getItemCount()][2];
        for (int i = 0; i < getItemCount(); i++){
            for (int x = 0; x < arrayNormalColors[i].length; x++){
                arrayNormalColors[i][x] = (String) cardModels.get(i).getColors()[x];
                arrayPressedColors[i][x] = (String) cardModels.get(i).getColors()[x];
            }
            hashMap.put(arrayNormalColors[i][0], arrayNormalColors[i][1]);
        }

        Set set = hashMap.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
            Log.d("NormalColors", mentry.getKey() + " : "+ mentry.getValue());
        }
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ItemHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_item, viewGroup, false));
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        CardModel cardModel = cardModels.get(i);
        ItemHolder holder = (ItemHolder) viewHolder;
        holder.cardTitle.setText(cardModel.getTitle());
        holder.imageView.setImageResource(cardModel.getIcon());
        holder.bind(holder, i);
    }


    private String first(String[][] array, int i){
        return array[i][0];
    }

    private String second(String[][] array, int i){
        return array[i][1];
    }

    public class ItemHolder extends RecyclerView.ViewHolder {

        LinearLayout linearCard;
        ImageView imageView;
        TextView cardTitle;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);

            linearCard = (LinearLayout) itemView.findViewById(R.id.linearCard);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            cardTitle = (TextView) itemView.findViewById(R.id.cardTitle);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        }

        private void bind(ItemHolder holder, int position){
            CardLayerDrawable.fit(itemView.getContext())
                    .setView(holder.linearCard)
                    .setBackgroundColorType(BackgroundColorType.GRADIENT)
                    .setNormalColors(new int[] {
                            Color.parseColor(first(arrayNormalColors, position)), //arrayNormalColors[position][0]
                            Color.parseColor(second(arrayNormalColors, position)) //arrayNormalColors[position][1]
                    })
                    .setPressedColors(new int[] {
                            Color.parseColor(second(arrayPressedColors, position)), //arrayPressedColors[position][1]
                            Color.parseColor(first(arrayPressedColors, position)) //arrayPressedColors[position][0]
                    })
                    .setGradientRadius(1f)
                    .setCornersRadius(13.333f)
                    .setGradientType(GradientType.LINEAR_GRADIENT)
                    .setOrientationType(OrientationType.RIGHT_LEFT)
                    .setShapeType(ShapeType.RECTANGLE)
                    .setStokeWidth(0)
                    .setStokeColor(Color.parseColor("#F2F2F2"))
                    .setBackgroundColor(Color.parseColor(first(arrayNormalColors, position)))
                    .setBorderSolidColor(Color.parseColor(second(arrayNormalColors, position)))//d1d5da
                    .setBorderGradientColor(new int[] {
                            Color.parseColor(second(arrayPressedColors, position)),
                            Color.parseColor(second(arrayPressedColors, position))
                    })//d1d5da
                    .setRippleColor(Color.parseColor(second(arrayPressedColors, position)))
                    .setLeft(0)
                    .setTop(0)
                    .setRight(0)
                    .setBottom(0)
                    .draw();
        }
    }

    @Override
    public int getItemCount() {
        return cardModels.size();
    }
}
