package com.medstudioinc.carddrawable;

public class CardModel {
    public int id;
    public int icon;
    public String title;
    public Object[] stringColors;


    public CardModel() {
    }

    public CardModel(int index, String title) {
        this.id = index;
        this.title = title;
    }

    public CardModel(int index, int icon, String title) {
        this.id = index;
        this.icon = icon;
        this.title = title;
    }

    public CardModel(int index, String title, Object[] colors) {
        this.id = index;
        this.title = title;
        this.stringColors = colors;
    }

    public CardModel(int index, int icon, String title, Object[] colors) {
        this.id = index;
        this.icon = icon;
        this.title = title;
        this.stringColors = colors;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public Object[] getColors() {
        return stringColors;
    }

    public void setColors(Object[] colors) {
        this.stringColors = colors;
    }
}
