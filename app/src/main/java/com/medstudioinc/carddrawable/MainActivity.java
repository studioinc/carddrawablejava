package com.medstudioinc.carddrawable;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;



import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView rv;
    CardAdapter cardAdapter;
    ArrayList<CardModel> cardModels = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rv = findViewById(R.id.rv);

        cardAdapter = new CardAdapter();

        rv.setLayoutManager(new GridLayoutManager(this, 2));
        rv.setAdapter(cardAdapter);

        cardModels.add(new CardModel(0, R.drawable.ic_android_black_24dp, "Quotes", new String[]{"#A5CC82", "#00467F"}));
        cardModels.add(new CardModel(1, R.drawable.ic_android_black_24dp, "Flags", new String[]{"#FF0099", "#493240"}));
        cardModels.add(new CardModel(2, R.drawable.ic_android_black_24dp, "Favorite", new String[]{"#f5af19", "#f12711"}));
        cardModels.add(new CardModel(3, R.drawable.ic_android_black_24dp, "Calender", new String[]{"#ACBB78", "#799F0C"}));
        cardModels.add(new CardModel(4, R.drawable.ic_android_black_24dp, "Lifestyle", new String[]{"#ffd452", "#544a7d"}));
        cardModels.add(new CardModel(5, R.drawable.ic_android_black_24dp, "People", new String[]{"#F7BB97", "#DD5E89"}));
        cardModels.add(new CardModel(6, R.drawable.ic_android_black_24dp, "Picture", new String[]{"#35beb2", "#136a8a"}));
        cardModels.add(new CardModel(7, R.drawable.ic_android_black_24dp, "Music", new String[]{"#12c2e9", "#c471ed"}));

        cardAdapter.addCards(cardModels);
    }
}
